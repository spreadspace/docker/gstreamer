#!/bin/bash

echo ""
echo "test:"
echo ""
echo "# gst-launch-1.0 decklinkvideosrc mode='1080p50' ! fakesink
echo ""
echo "  If input mode ist correct the log should contain a line like: 'Input source detected'"
echo ""
echo "record:"
echo ""
echo "# gst-launch-1.0 decklinkvideosrc device-number=0 mode='1080p50' ! autovideoconvert ! x264enc ! queue ! mux.  decklinkaudiosrc device-number=0 channels=2 ! audioconvert ! opusenc ! queue ! mux.   matroskamux name=mux ! filesink location=/srv/out/output.mkv"
echo ""
exec docker run --name gst-test -it --rm --cap-add=SYS_NICE \
       --device=/dev/blackmagic:/dev/blackmagic  \
       -v /usr/lib/blackmagic:/usr/lib/blackmagic \
       -v /usr/lib/libDeckLinkAPI.so:/usr/lib/libDeckLinkAPI.so \
       -v /usr/lib/libDeckLinkPreviewAPI.so:/usr/lib/libDeckLinkPreviewAPI.so \
       -v /tmp/out:/srv/out spreadspace/gstreamer bash
