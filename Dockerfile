ARG DISTRO
ARG CODENAME
FROM registry.gitlab.com/spreadspace/docker/foundation/${DISTRO}:${CODENAME}

RUN set -x \
    && apt-get update -q \
    && apt-get install -y -q gstreamer1.0-tools gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly python3 \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER app
